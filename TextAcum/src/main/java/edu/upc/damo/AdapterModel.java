package edu.upc.damo;

import java.util.Iterator;

/**
 * Created by Josep M on 02/11/2015.
 */
public class AdapterModel implements Iterable<CharSequence>,
                                     Vista.Visualitzable {



    //  -----------------------------  Comportament d'observable

    public interface OnCanviModelListener {
        public void onNovesDades();
    }

    private OnCanviModelListener observador;

    private Model model;

    public AdapterModel(Model model) {
        this.model = model;
    }
    public AdapterModel() {
        model = new Model();
    }

    public void setOnCanviModelListener(OnCanviModelListener observador) {
        this.observador = observador;
    }

    private void avisaObservador() {
        if (observador!=null)
            observador.onNovesDades();
    }

    // Delegació en l'element embolcallat

    public void afegir(CharSequence s) {
        model.afegir(s);
        avisaObservador();
    }

    /**
     * @param pos Posicio dins del model; base zero
     */
    public void remove(int pos) {
        model.remove(pos);
        avisaObservador();
    }

    public void buida(){
        model.buida();
        avisaObservador();
    }


    public boolean buit() {
        return model.buit();
    }

    @Override
    public Iterator<CharSequence> iterator() {
        return model.iterator();
    }

   // ------------------------- Visualitzable


    @Override
    public int quants() {
        return model.quants();
    }

    @Override
    public Object elem(int i) {
        return model.elem(i);
    }
}
