package edu.upc.damo;

import android.app.Activity;
import android.widget.TextView;

/**
 * Created by Josep M on 09/10/2014.
 * <p/>
 * Responsable de la presentació
 * Observa el model i s'actualitza quanaquest l'avisa
 */
public class Vista implements MainActivity.OnCanviModelListener {
     public interface Visualitzable{
        int quants();
        Object elem(int i);
    }

    private TextView res;
    private AdapterModel model;

    public Vista(Activity a) {
        res = (TextView) a.findViewById(R.id.resultat);
        ((MainActivity) a).setOnCanviModelListener(new MainActivity.OnCanviModelListener() {
            @Override
            public void onCanviModel() {
                refesPresentacio();
            }
        });
    }

    public void defineixVisualitzable(AdapterModel modelVisualitzable) {
        model = modelVisualitzable;
    }



    public void refesPresentacio() {
        if (model==null) return;

        res.setText("");
        for (int i=0; i<model.quants(); i++){
            if (i != 0)
                res.append("\n");
            res.append(String.valueOf(i + 1));
            res.append(" ");
            res.append(model.elem(i).toString());
        }
    }

    @Override
    public void onCanviModel() {
        refesPresentacio();
    }
}
